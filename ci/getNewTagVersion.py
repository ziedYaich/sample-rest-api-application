import requests
import argparse
import urllib3
import sys
import json
import os

#bypass SSL warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

parser=argparse.ArgumentParser()
parser.add_argument('--versioning', help='Semantic Versioning type (major, minor, batch)')
args=parser.parse_args()

def getNewTagVersion():
    headers = {
    'JOB-TOKEN': os.environ['CI_JOB_TOKEN']
    }
    response = requests.request('GET', 
    "{}/projects/{}/repository/tags?order_by=name".format(os.environ['CI_API_V4_URL'],
    os.environ['CI_PROJECT_ID'], headers=headers, verify=False))
    latestTag = json.loads(str(response.text))[0]['name']
    if args.versioning in ["major", "minor", "batch"]:
        newTagVersion = ""
        versionArray = latestTag.replace("v", "").split(".")
        if args.versioning == "major":
            newTagVersion = "v{}.{}.{}".format(int(versionArray[0]) + 1, 0, 0)
        elif args.versioning == "minor":
            newTagVersion = "v{}.{}.{}".format(versionArray[0], int(versionArray[1]) + 1, 0)
        else:
            newTagVersion = "v{}.{}.{}".format(versionArray[0], versionArray[1], int(versionArray[2]) + 1)
        print(newTagVersion)
    else:
        sys.exit("ERROR: The specified versioning is not one of the accepted options: (major, minor, batch)")

getNewTagVersion()
