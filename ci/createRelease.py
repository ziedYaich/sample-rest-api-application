import requests
import urllib3
import json
import os

#bypass SSL warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def createRelease():
    headers = {
    'JOB-TOKEN': os.environ['CI_JOB_TOKEN']
    }
    releaseName = "Release {}".format(os.environ['TAG'])
    releaseDescription = "Created release {}".format(os.environ['TAG'])
    data ={'name': releaseName, 'description': releaseDescription, 'tag_name': os.environ['TAG'], 'ref':os.environ['CI_COMMIT_SHA']}
    response = requests.request('POST', "{}/projects/{}/releases".format(os.environ['CI_API_V4_URL'], os.environ['CI_PROJECT_ID']),data= data, headers=headers)
    return response.text

createRelease()
